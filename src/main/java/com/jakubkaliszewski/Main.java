package com.jakubkaliszewski;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application
{
    public static void main( String[] args )
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader loader = new FXMLLoader();
        Scene scene = null;
        try{
            loader.setLocation(this.getClass().getClassLoader().getResource("./fxml/StackPaneWindow.fxml"));
            StackPane stackPane = loader.load();
            scene = new Scene(stackPane);
        } catch (Exception e) {
            System.out.println("Error: "+ e.getMessage());
            System.exit(1);
        }

        primaryStage.setScene(scene);
        primaryStage.setTitle("Learning English Fx");
        primaryStage.show();
    }
}
