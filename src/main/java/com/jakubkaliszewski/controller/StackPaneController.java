package com.jakubkaliszewski.controller;
import javafx.event.ActionEvent;

import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class StackPaneController {

    @FXML
    private Button button1;
    private int counter = 0;

    public StackPaneController() {
        System.out.println("Controller's constructor!");
    }

    @FXML
    void initialize(){//after constuctor
        button1.setText("This text was created in code.");
    }

    @FXML
    void onActionButton(ActionEvent event){
        System.out.println(((Button)event.getSource()).getId());
        button1.setText("Clicked!");
    }

    @FXML
    void onMouseEntered(){
        counter++;
        System.out.println("onMouseEntered! " + counter);
    }

}
